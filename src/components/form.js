import React from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";

const validationschema = Yup.object({
  uname: Yup.string().required("Required"),
  radiobtn: Yup.string().required("Required").nullable(),
  email: Yup.string()
    .required("Required")
    .matches(
      /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
      "Invalid email format"
    ),
  pword: Yup.string()
    .required("Required")
    .min(8, "Password is too short - should be 8 chars minimum.")
    .matches(/^[A-Z0-9]+$/i, "Password can only contain letters and numbers"),
});

function Form() {
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({
    mode: "all",
    defaultValues: {
      uname: "",
      radiobtn: "",
      email: "",
      pword: "",
    },
    resolver: yupResolver(validationschema),
  });
  const onSubmit = (data) => {
    console.log(data);
  };
  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        <label>
          Username:
          <input type="text" id="uname" name="uname" {...register("uname")} />
        </label>
        <p>{errors.uname?.message}</p>
        <br></br>
        Gender:
        <label>
          <input
            type="radio"
            value="male"
            name="radiobtn"
            {...register("radiobtn")}
          />
          Male
        </label>
        <label>
          <input
            type="radio"
            value="female"
            name="radiobtn"
            {...register("radiobtn")}
          />
          Female
        </label>
        <p>{errors.radiobtn?.message}</p>
        <br></br>
        <label>
          Email:
          <input type="email" id="email" name="email" {...register("email")} />
          {/* direct way to disply validation
            <input
            type="email"
            id="email"
            name="email"
            {...register(
              "email"
              , {
                required: { value: true, message: "Field is required!!" },
                maxLength: {
                  value: 20,
                  message: "Maximum 20 allowed",
                },
              }
            )}
          /> */}
        </label>
        <p>{errors.email?.message}</p>
        <br></br>
        <label>
          Password:
          <input
            type="password"
            id="pword"
            name="pword"
            {...register("pword")}
          />
        </label>
        <p>{errors.pword?.message}</p>
        <br></br>
        <button type="submit">Submit</button>
        <button
          type="reset"
          onClick={() => {
            reset();
          }}
        >
          Reset
        </button>
      </form>
    </>
  );
}

export default Form;
