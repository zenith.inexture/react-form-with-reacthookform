import "./App.css";
import Form from "./components/form";
import FormHeading from "./components/formheading";

function App() {
  return (
    <div className="App">
      <FormHeading />
      <Form />
    </div>
  );
}

export default App;
